# -*- coding: utf-8 -*-
import time
import datetime


def check_algoritmo(archivo):
    '''
    Función que lee el .txt de un certificado o una LCR y 
    retorna el algoritmo de cifrado
    '''
    datafile = file(archivo)    
    for line in datafile:
        if line.__contains__("Signature Algorithm: "):
            splitalgoritmo = line.split("Signature Algorithm: ")
            campo = splitalgoritmo[1]
            if campo.__contains__("WithRSAEncryption"):
                spliteado = campo.split("WithRSAEncryption")
                algoritmo = spliteado[0]
                break
    return algoritmo

def formatear_fecha_gtm(cadena):
    '''
    Función que retorna una fecha de forma 
    'Mar 25 14:55:00 2011' en '2011-03-25 14:55:00'
    '''
    fecha = datetime.datetime.strptime(cadena, '%b %d %H:%M:%S %Y')

    return fecha




def check_fecha(archivo, selector):

    datafile = file(archivo)  
    for line in datafile:
        # Fecha selector              
        if line.__contains__(selector):
            splitbefore = line.split(selector)
            campo2 = splitbefore[1]
            if campo2.__contains__(" GMT"):
                spliteado = campo2.split(" GMT")
                fecha_ini = spliteado[0]
                #print fecha_ini
                fecha = formatear_fecha_gtm(fecha_ini)
            break
    return fecha

'''
def consulta():
    archivo = raw_input("Ingrese la ruta del archivo: ")
    datafile = file(archivo)    

    print check_algoritmo(datafile)
    #check_fecha(datafile, "Not Before: ", "Not After : ")
    check_fecha(datafile, "Last Update: ","Next Update: ")
'''
#consulta()    
