from django.db import models


class NombreAbstract(models.Model):
    '''
    Modelo abstracto para la creacion de clases con campos 'nombre'
    '''
    nombre = models.CharField(max_length=512)

    class Meta:
        abstract = True

    def __unicode__(self):
        return u'%s' % (self.nombre)


