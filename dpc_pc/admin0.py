from django.contrib import admin
from lcr.models import Lcr, FormatoLcr

# Admin LCR -------------------->

class FormatoLcrInline(admin.TabularInline):
    model = FormatoLcr
    extra = 0


class LcrAdmin(admin.ModelAdmin):
    list_display = ('cert_lrc',)
    list_filter = ['cert_lrc', ]
    inlines = [FormatoLcrInline]
admin.site.register(Lcr, LcrAdmin)
