from django.shortcuts import render
from django.views.generic.base import View
from dpc_pc.models import DPC, PC
from certificados.models import Certificados, Proveedores

class DpcPcView(View):
    template = 'acraiz/dpc.html'
    diccionario = {}

    def get(self, request, *args, **kwargs):
        #FormatoCert.objects.filter(format_certificado__ac_cert__id = id_ac).order_by('-format_certificado__fecha_status')
        #dpc = DPC.objects.all()
        #pc = PC.objects.all().order_by('-fecha_publicacion')
        #self.diccionario.update({'dpc':dpc})
        #self.diccionario.update({'pc':pc})
        proveedores = Proveedores.objects.all()
        self.diccionario.update({'proveedores':proveedores})


        return render(request,
            template_name = self.template,
            dictionary = self.diccionario,
        )
