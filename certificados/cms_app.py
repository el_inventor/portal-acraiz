from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class CertificadosValidosApp(CMSApp):
    name = _('CertificadosValidos')
    urls = ['certificados.urls',]
    app_name = 'certificados'
    #menus = []

apphook_pool.register(CertificadosValidosApp)


