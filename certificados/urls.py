# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import CertificadosValidosView

urlpatterns = patterns('',
    url(r'^', CertificadosValidosView.as_view()),
)
