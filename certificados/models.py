# -*- coding: utf-8 -*-
# import Django
import os
from django.db import models

# import project packages
from core.models import NombreAbstract
from secciones.models import Categorias

# pre_save
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

#from subprocess import call
import subprocess

# Librerías y funciones 
from lib.funciones import check_algoritmo, check_fecha

class Proveedores(models.Model):
    '''
    Modelo para definir los datos de un PSC
    '''
    nombre = models.CharField(max_length= 100, verbose_name=u"Nombre del PSC")
    acronimo = models.CharField(max_length=100, verbose_name=u"Acrónimo del proveedor",
    help_text=u" Este acrónimo se usará para generar la ruta de los documentos para este proveedor. <br/> Ej: Super Intendencia de Cervicios de Certificación Electrónica. Acrónimo: SUSCERTE."
    )
    publicar = models.BooleanField(default=False,
            verbose_name=u'Publicar datos del proveedor', help_text=u"Publicar o despublicar DPC's/PC's, Certificados, Lcr, etc. de todas las AC's de este proveedor")
    logo = models.FileField(upload_to = 'LOGOS', blank=True)


    class Meta:
        verbose_name = u'proveedor'
        verbose_name_plural = u'proveedores'

    def __unicode__(self):
        return u'%s' % (self.nombre)

@receiver(pre_save, sender=Proveedores)
def pre_acronimo(sender, instance, **kwargs):
    acroniom_mayus = instance.acronimo.upper().replace(' ', '')
    instance.acronimo = acroniom_mayus


class AutoridadesCertificadoras(models.Model):
    categoria = models.ForeignKey(Categorias)
    nombre = models.CharField(max_length=100,
            verbose_name=u'Nombre de la AC')
    proveedor = models.ForeignKey(Proveedores, 
                )
    publicar = models.BooleanField(default=False,
            verbose_name=u'Publicar datos de la AC', help_text=u"Publicar o despublicar TODOS los datos de esta Autoridad de Certificación.")
    publicar_dpc = models.BooleanField(default=False,
            verbose_name=u'Publicar DPC', help_text=u"Publicar o despublicar DPC para esta Autoridad de Certificación.")


    class Meta:
        verbose_name = u'Autoridad Certificadora'
        verbose_name_plural = u'Autoridades Certificadoras (AC)'

    def __unicode__(self):
        return '%s' % (self.nombre)


class TipoFormato(NombreAbstract):
    '''
    Lista de Formatos de documentos disponibles
    '''

    class Meta:
        verbose_name = u'Tipo de formato'
        verbose_name_plural = u'Tipos de formatos'


class StatusCertificados(NombreAbstract):
    '''
    Lista de estatus de vaildez de un certificado
    '''

    class Meta:
        verbose_name = u'Status'
        verbose_name_plural = 'Status'


class CertificadoAbstract(models.Model):
    '''
    Datos de un certificado
    '''

    ac = models.ForeignKey(AutoridadesCertificadoras,
            verbose_name=u'AC',
            help_text=u'AC propietaria del documento')
    algoritmo = models.CharField( max_length=10, verbose_name=u'algoritmo de certificado',
            help_text=u'Algoritmo de cifrado del documento', blank=True)
    fecha_inicio = models.DateField(verbose_name=u'fecha de generación',
            help_text=u'Fecha de generación del documento', blank=True, null=True)
    fecha_fin = models.DateField(verbose_name=u'fecha de vencimiento',
            help_text=u'Fecha de vencimiento del documento', blank=True, null=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return u'%s %s %s' % (self.ac, self.fecha_inicio, self.etiqueta)

    def __unicode__(self):
        return u'%s (%s)' % (self.ac, self.algoritmo)

    def get_formatos(self):
        import pdb
        #pdb.set_trace()
        formatos = []
        formato_cert = FormatoCert.objects.filter(formato=self)
        [formatos.append(v.__unicode__()) for v in formato_cert]
        return formatos


class Certificados(CertificadoAbstract):
    etiqueta = models.CharField(max_length=1000,
            verbose_name=u'Nombre del documeto',
            help_text=u'Etiqueta del documento. Ej: Certificado de Persona Natural')
    status = models.ForeignKey(StatusCertificados,
            verbose_name=u'Status',
            help_text=u'estatus de este certificado')

    publicar = models.BooleanField(default=False,
            verbose_name=u'Publicar PC', help_text=u"Publicar o despublicar todas las PC's vinculadas a este certificado")

    class Meta:
        verbose_name = u'certificado'
        verbose_name_plural = 'certificados'



class FormatoCert(models.Model):
    ''' Diferentes formatos disponibles para cada certificado '''

    def get_image_path(self, filename):
        
        autoridad = (self.certificado.ac.proveedor.acronimo)
        ruta = 'CERTIFICADOS/%s' % (autoridad)
        #print ruta
        return os.path.join( ruta, filename)

    certificado = models.ForeignKey(Certificados)
    formato = models.ForeignKey(TipoFormato,
            verbose_name=u'Formato',
            help_text=u'Formato que en el que se publicará certificado')

    archivo = models.FileField(upload_to = get_image_path)
    huella = models.FileField(upload_to = get_image_path)

    class Meta:
        verbose_name = u'Formatos de Certificado'
        verbose_name_plural = u'Formatos de Certificado'

    def get_formatos(self):
        formatos = []
        formato_cert = FormatoCert.objects.filter(formato=self)
        [formatos.append(v.__unicode__()) for v in formato_cert]
        return formatos

    def __unicode__(self):
        return '%s' % (self.formato)

@receiver(post_save, sender=FormatoCert)
def post_handler(sender, instance, **kwargs):
    # Actualizo el algoritmo
    archivo_nombre = str(instance.archivo)
    ruta = 'media/'+archivo_nombre

    if archivo_nombre.__contains__('.txt'):
        instance.certificado.algoritmo = check_algoritmo(ruta)
        instance.certificado.fecha_inicio = check_fecha(ruta,"Not Before: ")
        instance.certificado.fecha_fin = check_fecha(ruta,"Not After : ")
        instance.certificado.save(update_fields=["fecha_inicio"])
        instance.certificado.save(update_fields=["fecha_fin"])
        instance.certificado.save(update_fields=["algoritmo"])

    