# -*- coding: utf-8 -*-
from django.contrib import admin
from secciones.models import Categorias, OrdenCategoria


class CategoriasAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Datos de la autoridad',
            {'fields': [
                'nombre_autoridad',
                'orden_categoria',
                'publicar_cat']}),
    ]
admin.site.register(Categorias, CategoriasAdmin)
admin.site.register(OrdenCategoria)
