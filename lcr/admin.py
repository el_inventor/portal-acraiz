from django.contrib import admin
from lcr.models import Lcr, FormatoLcr, TipoCert, TipoFormato


admin.site.register(TipoCert)
admin.site.register(TipoFormato)
admin.site.register(FormatoLcr)


class FormatoLcrInline(admin.TabularInline):
    model = FormatoLcr
    extra = 0


class LcrAdmin(admin.ModelAdmin):
    fields = ('ac', 'publicar')
    list_display = ('ac','algoritmo', 'fecha_inicio', 'fecha_fin', 'publicar')
    list_filter = ('ac' ,'algoritmo', 'fecha_inicio', 'fecha_fin', 'publicar')
    inlines = [FormatoLcrInline]
admin.site.register(Lcr, LcrAdmin)
