Certificados Válidos
====================

* Agregar unique=True al orden de categoria [LISTO]

* Añadir fecha_inicio, fecha_fin, eliminar booleano principal [LISTO]

* Sustituir la huella de tipo Filer a CharField [LISTO]

* Cambiar app_name de Secciones a Certificados Validos [LISTO]

* Tomar ruta www.example.com/ocsp/ para servir archivo ocsp
* Tomar ruta www.example.com/lcr/ para servir archivo lcr

* Cambiar orden de categoria a integerfield [LISTO]

* Cambiar DPC de foreign key a filerfield en Autoridad Certificadora [LISTO]

* [LISTO] Añadir booleano de Publicar DPC

* [LISTO] Separar PC de Certificado y añadirle un Inline en el admin (como los Formatos de Certificado)

* [LISTO] Corregir bloque de secciones para proveedores [LISTO]

* [LISTO] Validar NO publicación de AC si no tiene certificados válidos


=================


